import React from 'react';
import { ActivityIndicator } from 'react-native';
import { Overlay, Text } from 'react-native-elements';
import styles from '../styles/LoadingStyle'
import PropTypes from 'prop-types';

const Loading = () => {
    return (
        <Overlay
            containerStyle={styles.loadingContainer}
            overlayStyle={styles.loadingOverlay}
            isVisible={true}>
            <ActivityIndicator size={45} color="#333" />
        </Overlay>
    );
}

export default Loading;
