import gql from 'graphql-tag';

export const getPoints = gql`
    query{
        getPoints{
            id
            title
            latitude
            longitude
            description
        }
    }
`
