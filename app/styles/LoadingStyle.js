
import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
    loadingContainer: {
        backgroundColor: "rgba(50, 50, 50, 0.6)",
    },
    loadingOverlay: {
        backgroundColor: "rgba(0, 0, 0, 0)",
        width: "auto",
        height: "auto"
    }
});

export default style;
