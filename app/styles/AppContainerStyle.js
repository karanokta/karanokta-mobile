import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
    appContainer: {
        ...StyleSheet.absoluteFillObject,
    },
});

export default style;
