import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
    mapContainer: {
        ...StyleSheet.absoluteFillObject,
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    clusterContainer: {
        width: 30,
        height: 30,
        padding: 6,
        borderWidth: 1,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(50, 50, 50, 0.9)',
    },
    clusterText: {
        fontSize: 13,
        color: '#fff',
        fontWeight: '500',
        textAlign: 'center',
    }
});

export default style;
