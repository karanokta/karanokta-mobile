import React, { Component } from 'react';
import {
  Platform,
  View
} from 'react-native';

import { Header, Overlay, Text } from 'react-native-elements';
import styles from './styles/AppStyle'
import AppContainer from './containers/AppContainer'
import Map from './containers/Map'

import ApolloClient from "apollo-boost";
import { ApolloProvider } from 'react-apollo';

const client = new ApolloClient({
      uri: "http://138.197.109.61:8080/graphql"
});

export default class App extends Component<Props> {
    render() {
        return (
            <ApolloProvider client={client}>
                <View style={styles.container}>
                    <Map />
                    <AppContainer />
                </View>
            </ApolloProvider>
        );
    }
}

