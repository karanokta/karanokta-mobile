import React, { Component} from 'react';

import {
  Text,
  View
} from 'react-native';

import { Marker, Callout } from 'react-native-maps';
import ClusteredMapView from 'react-native-maps-super-cluster'

import { getPoints } from '../queries'
import { graphql, withApollo, compose } from 'react-apollo';
import { Loading } from '../components';

import styles from '../styles/MapStyle'

class Map extends Component {
    constructor(props){
        super(props);
        this.state = {
            points:  [],
            loading: true
        }
    }
    componentDidMount(){
        this.props.client.query({
            query: getPoints
        }).then(({data: {getPoints}}) => {
            this.setState({
                points: getPoints.map(({id, title, description, latitude, longitude}) => ({
                    id, title, description,
                    location: { latitude, longitude }
                })),
                loading: false
            })
        })
    }
    renderMarker = (point) => (
        <Marker identifier={`point-${point.id}`} key={point.id} coordinate={point.location} />
    )
    renderCluster = (cluster, onPress) => {
        const pointCount = cluster.pointCount,
              coordinate = cluster.coordinate,
              clusterId = cluster.clusterId
        const clusteringEngine = this.map.getClusteringEngine(),
              clusteredPoints = clusteringEngine.getLeaves(clusterId, 100);

        return (
            <Marker coordinate={coordinate} onPress={onPress}>
                <View style={styles.clusterContainer}>
                    <Text style={styles.clusterText}>
                        {pointCount}
                    </Text>
                </View>
            </Marker>
        )
    }
    render() {
        if(this.state.loading) return null;
        return (
            <ClusteredMapView
                style={{flex: 1}}
                data={this.state.points}
                initialRegion={{
                    latitude: 41.019777,
                    longitude: 28.973145,
                    latitudeDelta: 1.2,
                    longitudeDelta: 1.2
                }}
                ref={(r) => { this.map = r }}
                renderMarker={this.renderMarker}
                renderCluster={this.renderCluster} />
        );
    }
}

export default compose(
    withApollo
)(Map);
