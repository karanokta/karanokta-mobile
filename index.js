import { AppRegistry } from 'react-native';
import App from './app/App';

AppRegistry.registerComponent('karanokta_mobile', () => App);
